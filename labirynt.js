"use strict";

//AGK Kamil Dudek 1ID23B Lab01 

//Deklaracja stałych oraz zmiennych wykorzystywanych przez program
let c = document.getElementById("myCanvas");
let context = c.getContext("2d");

const WIDTH = 500;
const HEIGHT = 500;

// const CELL_WIDTH = 25;
// const CELL_HEIGHT = 25;
const CELL_HEIGHT_WIDTH = 25;
const CELL_WIDTH = CELL_HEIGHT_WIDTH;
const CELL_HEIGHT = CELL_HEIGHT_WIDTH;

const MAX_CELL_X = WIDTH / CELL_WIDTH;
const MAX_CELL_Y = HEIGHT / CELL_HEIGHT;

//Możliwe kierunki
const TOP = "TOP";
const BOTTOM = "BOTTOM";
const RIGHT = "RIGHT";
const LEFT = "LEFT";

//Wszystkie komórki labiryntu
let cells = [];
//Ścieżki wykorzystywane przy rozwiązywaniu labiryntu
let path = [];
let finishedPath = [];

//Funkcja usypiająca program
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

//Funkcja losująca kolejność elementów w tablicy
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

//Funkcja zwracająca zmianę pozycji w danym kierunku
function directionDeltaPos(direction) {
  switch (direction) {
    case TOP:
      return { dx: 0, dy: -1 };
    case BOTTOM:
      return { dx: 0, dy: 1 };
    case RIGHT:
      return { dx: 1, dy: 0 };
    case LEFT:
      return { dx: -1, dy: 0 };
    default:
      return null;
  }
}

//Funkcja zwracająca przeciwny kierunek
function oppositeDirection(direction) {
  switch (direction) {
    case TOP:
      return BOTTOM;
    case BOTTOM:
      return TOP;
    case RIGHT:
      return LEFT;
    case LEFT:
      return RIGHT;
    default:
      return null;
  }
}

//Funkcja sprawdzająca czy dane x i y znajdują się w dopuszczalnych wartościach
function checkSizeConstraints(x, y) {
  return x >= 0 && y >= 0 && x < MAX_CELL_X && y < MAX_CELL_Y;
}

//Klasa ściany
class Wall {
  constructor(direction) {
    //Kierunek w jakim ściana jest ustawiona
    this.direction = direction;
    //Zmienna definiująca czy ściana istnieje
    this.exists = true;
  }
  //Metoda rysująca ścianę
  draw(screenX, screenY) {
    if (this.exists) {
      switch (this.direction) {
        case TOP:
          context.moveTo(screenX, screenY);
          context.lineTo(screenX + CELL_WIDTH, screenY);
          break;
        case BOTTOM:
          context.moveTo(screenX, screenY + CELL_HEIGHT);
          context.lineTo(screenX + CELL_WIDTH, screenY + CELL_HEIGHT);
          break;
        case RIGHT:
          context.moveTo(screenX + CELL_WIDTH, screenY);
          context.lineTo(screenX + CELL_WIDTH, screenY + CELL_HEIGHT);
          break;
        case LEFT:
          context.moveTo(screenX, screenY);
          context.lineTo(screenX, screenY + CELL_HEIGHT);
          break;
        default:
          console.error(`Wall draw() error - x: ${screenX} y: ${screenY}`);
          break;
      }
      context.strokeStyle = "black";
      context.stroke();
    }
  }
}

//Klasa komórki labiryntuu
class Cell {
  //Ściany komórki
  walls = [new Wall(TOP), new Wall(RIGHT), new Wall(BOTTOM), new Wall(LEFT)];

  constructor(x, y) {
    //Zmienna określająca czy komórka jest startem
    this.start = false;
    //Zmienna określająca czy komórka jest końcem
    this.finish = false;
    //Zmienna określająca czy komórka była odwiedzona
    this.visited = false;
    //Zmienna określająca współrzędną logiczną X
    this.x = x;
    //Zmienna określająca współrzędną logiczną Y
    this.y = y;
    //Zmienna określająca współrzędną X na ekranie
    this.screenX = x * CELL_WIDTH;
    //Zmienna określająca współrzędną Y na ekranie
    this.screenY = y * CELL_HEIGHT;
  }
  //Metoda wyłączająca ścianę
  disableWall(direction) {
    let wall = this.walls.find(wall => wall.direction === direction);

    wall.exists = false;
  }
  //Metoda znajdująca i zwracająca ścianę
  findWall(direction) {
    let wall = this.walls.find(wall => wall.direction === direction);
    return wall;
  }
  //Metoda rysująca komórkę
  draw() {
    if (this.visited) {
      context.fillStyle = "#00FFFF";
    } else {
      context.fillStyle = "#FFFFFF";
    }

    if (this.start) context.fillStyle = "#0088FF";
    if (this.finish) context.fillStyle = "#8800FF";

    context.fillRect(this.screenX, this.screenY, CELL_WIDTH, CELL_HEIGHT);

    this.walls.map(wall => {
      if (wall.exists) {
        wall.draw(this.screenX, this.screenY);
      }
    });
  }
}

//Funkcja tworząca wszystkie możliwe komórki labiryntu i dodająca je do globalnej tabeli cells
function init() {
  for (let i = 0; i < MAX_CELL_X; i++) {
    for (let j = 0; j < MAX_CELL_Y; j++) {
      cells.push(new Cell(i, j));
    }
  }
}
//Funkcja rysująca wszystkie komórki w tabeli podanej przez parametr
function drawCells(cells) {
  cells.map(cell => cell.draw());
}

//Funkcja wykorzystywana przy animacji końcowej ścieżki
async function animateCells(cells) {
  for (let i = 0; i < cells.length; i++) {
    await sleep(10);
    cells[i].draw();
  }
}

//Funkcja resetująca atrybut visited wszystkich komórek
function resetCellsVisitedAttr() {
  cells.map(cell => (cell.visited = false));
}

//Rekurencyjna funkcja generująca labirynt; jako parametr przyjmuje komórkę labiryntu.
//Na początku komórka jest oznaczana jako zwiedzona, po czym tworzona jest tablica z kierunkami.
//Kolejność elementów tej tablicy jest losowana, a następnie dla każdego kierunku wykonywane są czynności:
// - ustalane są nowe współrzędne na bazie współrzędnych obecnej komórki oraz obecnego kierunku
// - znajdywana jest komórka o nowych współrzędnych, jeśli te spełniają warunek dotyczący dozwolonego zakresu wartości
// - jeśli nowa komórka nie została odwiedzona, wyłączane są odpowiednie ściany aby stworzyć przejście
// - następnie funkcja wywoływana jest rekurencyjnie dla nowej komórki
function generateMaze(cell) {
  cell.visited = true;

  const directions = [TOP, BOTTOM, RIGHT, LEFT];

  shuffle(directions);

  directions.map(direction => {
    const nextX = cell.x + directionDeltaPos(direction).dx;
    const nextY = cell.y + directionDeltaPos(direction).dy;
    let nextCell = {};
    if (checkSizeConstraints(nextX, nextY)) {
      nextCell = cells.find(cell => cell.x === nextX && cell.y === nextY);
    }

    if (nextCell.visited === false) {
      cell.disableWall(direction);
      nextCell.disableWall(oppositeDirection(direction));

      generateMaze(nextCell);
    }
  });
}

//Rekurencyjna funkcja rozwiązująca labirynt; jako parametr przyjmuje komórkę labiryntu
//Funkcja oznacza komórkę jako zwiedzoną, następnie sprawdza czy podana komórke nie jest końcem labiryntu.
//Jeśli komórka jest końcem labiryntu to funkcja ustala końcową ścieżkę oraz kończy swoje działanie
//W przeciwnym wypadku funkcja zachowuje się podobnie do tej tworzącej labirynt:
// - losuje tablicę kierunków
// - dla każdego kierunku znajduje nową komórkę na bazie pozycji obecnej komórki oraz obecnego kierunku
// - dodatkowo znajduję odpowiednią dla danego kierunku ścianę obecnej komórki
// - następnie funkcja sprawdza czy ściana nie istnieje (czy można przejść) oraz czy nowa komórka była już zwiedzona
// - jeśli warunki te są spełnione obecna komórka jest dodawana do zmiennej path, reprezentującej ścieżkę
// - następnie funkcja jest wywoływana rekurencyjnie z nową komórką
function solveMaze(cell) {
  cell.visited = true;

  if (cell.finish) {
    finishedPath = [...path];
    return;
  }

  const directions = [TOP, BOTTOM, RIGHT, LEFT];
  shuffle(directions);

  directions.map(direction => {
    const nextX = cell.x + directionDeltaPos(direction).dx;
    const nextY = cell.y + directionDeltaPos(direction).dy;
    if (checkSizeConstraints(nextX, nextY)) {
      let nextCell = cells.find(cell => cell.x === nextX && cell.y === nextY);
      const wall = cell.findWall(direction);

      if (!wall.exists && !nextCell.visited) {
        path.push(cell);
        solveMaze(nextCell);
        path.pop();
      }
    }
  });
}

//Główna funkcja uruchamiająca wszystkie poprzednie
//Ustawia początkowy oraz końcowy punkt labiryntu (pierwszą oraz ostatnią komórkę)
function maze() {
  init();
  cells[0].start = true;
  cells[cells.length - 1].finish = true;
  generateMaze(cells.find(cell => cell.start));

  resetCellsVisitedAttr();
  drawCells(cells);
  solveMaze(cells.find(cell => cell.start));
  animateCells(finishedPath);
}

maze();
